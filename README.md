# 

Aqui você poderá baixar todos os códigos realizados em sala de aula

## Para começar

Baixe o GIT no seu computador e Node.js (caso esteja no IFMS não precisa fazer nada disso)

## Selecionando a aula

Abra o terminal no seu computador

```
git clone https://gitlab.com/ifms-aulas/moveis.git
cd moveis
npm install
```

## Executar o projeto

npx expo start --web

## Slides

Não será utilizado os slides, e sim anotaçoes de aulas que serão colocadas na pastas slides, na raiz desse projeto.

## Fazendo...

[//]: # (- [ ] lpat-48Vtxjf1DBBQX_Eg9siB)
