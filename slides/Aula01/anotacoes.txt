O que é React Native?
React Native é uma framework de código aberto criada pelo Facebook que permite o desenvolvimento de aplicativos móveis
utilizando JavaScript/Typescript e React. A principal vantagem do React Native é a capacidade de desenvolver para iOS e
Android com uma única base de código, facilitando o desenvolvimento e a manutenção de aplicativos.

Componentes Básicos de Tela
View
O componente View é o mais fundamental dos componentes de layout no React Native. Ele serve como um contêiner que suporta
estilização e layout com Flexbox. Você pode pensar nele como equivalente a uma div no HTML. É usado para agrupar outros
componentes ou como um contêiner para estilos de layout.

Text
O componente Text é usado para exibir texto. Ele suporta estilização básica como cor, fonte, tamanho, entre outros.
Todos os textos em um aplicativo React Native devem estar dentro de um componente Text.

TextInput
O TextInput é um componente que permite aos usuários inserir texto. É usado para formulários, entrada de dados e outros
casos onde a interação do usuário é necessária para capturar texto.

Visualizando o Projeto na Web com Expo
Expo é uma ferramenta que facilita o desenvolvimento com React Native, permitindo que você execute seu projeto em
dispositivos físicos, emuladores e na web. Para visualizar seu projeto React Native na web, você pode usar o comando
npx expo start --web.
Este comando inicia o servidor de desenvolvimento do Expo e abre uma aba no navegador para visualizar o aplicativo.

O que é package.json?
O package.json é um arquivo fundamental em projetos Node.js, incluindo projetos React Native. Ele contém metadados sobre
o projeto e uma lista de dependências necessárias para o projeto ser executado. Além disso, define scripts que podem ser
executados para iniciar, construir, testar, entre outras ações no projeto.

Arquivo Principal App.tsx
O App.tsx é o ponto de entrada padrão para aplicativos React Native (quando se usa TypeScript; para JavaScript seria App.js).
Este arquivo é onde você define o componente principal que será renderizado pelo seu aplicativo. Normalmente, outros
componentes são importados e utilizados dentro deste arquivo para estruturar a interface do usuário do aplicativo.

Pasta Assets
A pasta assets é onde você armazena recursos estáticos do seu projeto, como imagens, fontes, sons, etc. Esses recursos
podem ser referenciados e utilizados em seu aplicativo. Por exemplo, você pode importar uma imagem da pasta assets para
ser usada como um ícone ou fundo em seu aplicativo.

Conclusão
React Native é uma poderosa ferramenta para o desenvolvimento de aplicativos móveis, oferecendo componentes básicos
para a construção de interfaces de usuário e a possibilidade de visualização e teste em diversas plataformas.
Compreender os componentes básicos, como View, Text, e TextInput, além de ferramentas como Expo e estruturas do projeto,
como o package.json e o arquivo App.tsx, é fundamental para iniciar o desenvolvimento de aplicativos com React Native.