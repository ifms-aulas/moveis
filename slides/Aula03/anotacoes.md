[//]: # (Implementando a navegação de telas na Home)
No caso do nosso projeto, vamos utilizar a biblioteca React Navigation. 
Para usar, será necessário acessar o site reactnavigation.org a partir do seu navegador. Nessa página, clicamos no botão "Read Docs" 
(Ler documentação) no centro da tela para acessarmos a documentação da biblioteca.

Essa biblioteca React Navigation fornece algumas funcionalidades prontas, ou que precisamos fazer apenas uma breve 
alteração para construir rotas. Dessa maneira, facilita e acelera o processo de desenvolvimento.

Dentro da documentação, temos alguns textos, como os pré-requisitos, é necessário ser familiar com JavaScript, React e 
React Native. Também tem os requisitos mínimos, que o nosso projeto do Adopet já tem para seguir com essa biblioteca, 
mas é importante para visualizar caso queira aplicar essa biblioteca em outro projeto já existente.

[//]: # (Instalação da biblioteca)
Depois, ele chega nas instruções de instalação. Para isso, vamos precisar usar o comando: 

```
npm install @react-navigation/native 
```

```
npm install @react-navigation/stack 
```
```
npm install react-native-gesture-handler
```

Copiaremos esse comando e precisamos colocá-lo no terminal do VS Code. Para isso, vamos abrir novamente a IDE e, no menu 
superior, acessaremos a "Terminal > New Terminal". Também podemos usar o comando "Ctrl + Shift + '". Ele vai abrir um 
novo terminal, na parte inferior do nosso projeto, onde colamos e rodamos esse comando.

[//]: # (Instalando dependências)
Agora, vamos visualizar na documentação se falta alguma coisa. Então, a próxima sessão, ele já explica sobre instalar 
dependências em um projeto gerenciado pelo Expo. Esse é o nosso caso. Então, vamos precisar copiar mais um comando:

```
npx expo install react-native-screens react-native-safe-area-context
```

Agora, não estamos usando o npm, estamos usando o Expo para fazer essa instalação. E ele está instalando, primeiro, a 
biblioteca React Native Screens, que faz uma implementação nativa do fluxo de navegação. Isso acaba melhorando o 
desempenho da nossa aplicação por essa implementação ser nativa e é bem recomendado para quando temos uma aplicação com 
várias telas. 

Também, estamos instalando a biblioteca React Native Safe Area Context. Essa biblioteca faz a gestão de áreas seguras 
nas bordas do nosso dispositivo. Ela tenta fazer com que não haja sobreposição de barras de status e barras de navegação 
em cima do nosso aplicativo.

[//]: # (Criando o container de navegação)

Para isso, vamos acessar o nosso projeto no VSCode e, no explorador de arquivos, que fica na esquerda da nossa tela, 
vamos fechar a pasta "telas". Vamos clicar com o botão direito do mouse sobre a pasta "src" e 
selecionar em "New Folder" (Nova pasta), que é a segunda opção. Chamaremos a nova pasta de "rotas".

Clicaremos com o botão direito sobre a pasta "rotas" e, dessa vez, selecionaremos a opção "New File", que é a 
primeira opção. Nomearemos esse arquivo como navigation.js. Ao pressionarmos "Enter", abrimos o rotas.tsx, onde 
precisamos construir uma função.

Portanto, escreveremos export default function Navigation(){}, escrevendo Navigation com "N" maiúsculo. Dentro das chaves, 
escreveremos um return();. Essa é uma sintaxe já conhecida por nós, pessoas desenvolvedoras, especificamente de TypeScript, 
e agora precisamos retornar algo para construir esse fluxo de navegação.

No caso do React Navigation, precisamos utilizar o <NavigationContainer>. Ele já aparece no autoimport, então basta 
clicarmos na primeira opção para termos o import na primeira linha. Vamos fazer o fechamento dessa tag também: 
</NavigationContainer>.

```
import { NavigationContainer } from "@react-navigation/native";

export default function Navigation() {
    return (
    <NavigationContainer>
    
    </NavigationContainer>
    );
}
```

Por padrão, a nossa aplicação abre a tela de home. Agora, por que isso é renderizado quando abrimos pela primeira vez? 
O arquivo que o nosso projeto lê é o App.tsx, que aparece na lista de arquivos do explorador, à esquerda da tela, em 
"Login > App.tsx".

Ao abrirmos o App.tsx, dentro do return(), ele está para ser renderizado somente o nosso componente, que é a 
página <Login />.

Crie agora o arquivo Home.tsx na pasta "src/pages". Esse arquivo será a nossa página inicial após o Login, que será 
renderizada. Dentro desse arquivo, vamos criar um componente chamado HomeScreen, que será uma função que retorna um JSX.

```
import {Text, View} from "react-native";

function HomeScreen() {
    return (
        <View>
            <Text>Home</Text>
        </View>
    );
}

export default HomeScreen;
```

Agora voltamos para o arquivo rotas.tsx, que está na raiz do projeto. Vamos importar o componente HomeScreen
e, dentro do <NavigationContainer>, vamos criar um <Stack.Navigator>. Esse Stack.Navigator é um componente que
vem da biblioteca React Navigation. Ele é responsável por criar um conjunto de telas que podem ser navegadas.

```
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Login from "./pages/Login";
import HomeScreen from "./pages/Home";

const Stack = createStackNavigator();

export default function Rotas() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={"Login"}>
                <Stack.Screen name='Login' component={Login} />
                <Stack.Screen name='Home' component={HomeScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
```

Agora, precisamos voltar no arquivo Login.tsx para criar uma função que, ao clicar no botão, redirecione para a
página Home. Para isso, vamos importar o hook useNavigation, que é um hook que nos permite navegar entre telas.

```
import {useNavigation} from "@react-navigation/native";

function LoginScreen() {

    const navigation = useNavigation();

    function handleLogin() {
        navigation.navigate('Home');
    }
```

Após criar a função handleLogin, precisamos chamar essa função no botão de login. Para isso, vamos adicionar o
onPress={handleLogin} no botão de login e executar o projeto.

```
<Button title="Login" onPress={handleLogin} />
```